**Battleships game**
================
---
This was the 4th and final Java coursework and was a group programming exercise (3 people including myself).
 
More than 2/3 of the code was written by Teresa Wu & Qian Yunkan, Mar-April 2014.

With exams bearing down, the aim was fulfil the requirements asap and nothing more snazzy than that! (eg. no GUI).

A detailed description of the game's functional requirements were specified.
Furthermore, the requirements specified most of the classes to build and some of the methods to implement.
They are cut & pasted below:
---
The Game:
--------
The computer places ten ships on the ocean in such a way that no ships are immediately adjacent to each other, either horizontally, vertically, or diagonally. For example,
The human player does not know where the ships are. The initial display of the ocean shows a 10 by 10 grid of locations, all the same.

The human player tries to hit the ships, by calling out a row and column number.
The computer responds with either hit or miss.
When a ship is hit, but not sunk, the program does not provide any information about what kind of a ship was hit.
When a ship is hit and sinks, the program prints out a message "You just sank a ship-type ", where ship-type is the type of ship that was sunk.

After each shot, the computer redisplays the ocean with the new information.
A ship is sunk when every square of the ship has been hit. Thus, it takes:

5 hits (in 5 different places) to sink an aircraft carrier,
4 for a battleship,
3 for a submarine,
2 for a destroyer, and
1 for a patrol boat.

The aim is to sink the fleet with as few shots as possible. When all ships have been sunk, the program prints out a message that the game is over, and prints out how many shots were required.

The Fleet 
---------
The fleet consists of one aircraft carrier, two battleships, two submarines, two destroyers, and four patrol boats.

Details
-------
Name your project Battleship, and your package battleship.

The classes
------------
Your program should have the following classes:

* BattleshipGame - This is the main class, containing the main method and a variable of type Ocean.
* Ocean - This contains a 10x10 grid of Ships which represents the \ocean", and some methods to manipulate it.
* Ship - This describes characteristics common to all the ships. It has the following sub-classes:
* AircraftCarrier - describes a ship with length 5.
* Battleship - describes a ship with length 4.
* Submarine - describes a ship with length 3.
* Destroyer - describes a ship with length 2.
* PatrolBoat - describes a ship with length 1.
* EmptySea - Describes a part of the ocean that doesn't have a ship in it.

It may seem strange to have the lack of a ship be a type of ship, but this is a decision that simplies the design considerably as every location in the ocean contains a ship of some kind.
We will now consider the specication of each of these classes in-turn

class BattleshipGame
---------------------
In this class you will set up the game; accept shots from the user; display the results; print scores; and ask the user if s/he wants to play again.

All input/output is done here. All computation will be carried out in the Ocean class and the various Ship classes.
To aid the user, row numbers should be displayed along the left edge of the grid, and column numbers should be displayed along the top. Numbers should be 0 to 9, not 1 to 10. The top left corner square should be 0,0. 
Use different characters to indicate locations that contain a hit, locations that contain a miss, and locations that have never been red upon.

Use methods appropriately; don't cram everything into one or two methods, but try to divide up the work into sensible parts with reasonable names. Test every non-private method in the Ship class. Also, test the methods in each subclass of Ship.

class OceanTest
----------------
This is a JUnit test class for Ocean. Test every required method for Ocean, including the constructor, but not including the toString() method. If you create additonal methods in the Ocean class, you must either make them private, or write tests for them. 
Test methods do not need comments, unless they do something non-obvious.
Experience has shown that it is a bad idea for one person in the group to write the tests and another member to write the code. It works much better if you write tests for your own code.

class Ocean
------------
###Methods

* Ocean() - The constructor. Creates an empty ocean (lls the ships array with EmptySeas). Also initializes any game variables, such as how many shots have been red.
* void placeAllShipsRandomly() - Place all the ships randomly on the (initially empty) ocean. Place larger ships before smaller ones, or you may end up with no legal place to put a large ship. You will want to use the Random class in the java.util package, so look that up in the Java API.
* boolean isOccupied(int row, int column) - returns true if the given location contains a ship, false if it does not.
* boolean shootAt(int row, int column) - returns true if the given location contains a real ship, still afloat, (not an EmptySea), false if it does not. In addition, this method updates the number of shots that have been red, and the number of hits.
Note: If a location contains a real ship, shootAt should return true every time the user shoots at that same location. Once a ship has been sunk, additional shots at its location should return false.
* int getShotsFired() - returns the number of shots red (in this game).
* int getHitCount() - returns the number of hits recorded (in this game). All hits are counted, not just the rst time a given square is hit.
* int getShipsSunk() - returns the number of ships sunk (in this game).
* boolean isGameOver() - returns true if all the ships have been sunk, otherwise false.
* Ship[][] getShipArray() - returns the grid of ships.
* @ Override String toString() - returns a string representing the ocean.

To aid the user, row numbers should be displayed along the left edge of the array, and column numbers should be displayed along the top. Numbers should be 0 to 9, not 1 to 10. The top left corner square should be 0, 0.

	use 'S' to indicate a location that you have red upon and hit a (real) ship,
	use '-' to indicate a location that you have red upon and found nothing there,
	use 'x' to indication location containing a sunken ship, and
	use '.' to indicate a location that you have never red upon.

You are welcome to write additional methods of your own. Additional methods should either be tested (if you think they have some usefulness outside this class), or private (if they don't).

class Ship
----------
Since we don't really care which end of a ship is the bow and which the stern, we will consider all ships to be facing up, or left. Other parts of the ship are in higher-numbered rows or columns. You don't need to write a constructor for this class.

###Methods

* abstract int getLength() - returns the length of this particular ship. This method exists only to be overridden.
* int getBowRow()- returns bowRow
* int getBowColumn() - returns bowColumn
* boolean isHorizontal() - returns horizontal
* void setBowRow(int row)- sets the value of bowRow
* void setBowColumn(int column) - sets the value of bowColumn
* void setHorizontal(boolean horizontal) - sets the value of the instance variable horizontal.
* abstract String getShipType() - returns the type of this ship.
* boolean okToPlaceShipAt(int row, int column, boolean hori, Ocean ocean) - returns true if it is okay to put a ship of this length with its bow at this location, with the given orientation, and returns false otherwise.
The ship must not overlap another ship, or touch another ship (vertically, horizontally, or diagonally), and it must not stick out beyond the grid.
Does not actually change either the ship or the Ocean, just says whether it is legal to do so.
* void placeShipAt(int row, int column, boolean horizontal, Ocean ocean) - places the ship in the ocean. This involves giving values to the bowRow, bowColumn, and horizontal instance variables in the ship, and it also involves putting a reference to the ship in each of 1 or more locations (up to 4) in the ships grid in the Ocean object.
(Note: This will be as many as four identical references; you can't refer to a part of a ship, only to the whole ship.)
* boolean shootAt(int row, int column) - if a part of the ship occupies the given row and column, and the ship hasn't been sunk, mark that part of the ship as hit (in the hit array, where index 0 indicates the bow) and return true, otherwise return false.
* boolean isSunk() - return true if every part of the ship has been hit, false otherwise.

The remaining classes
---------------------
* AircraftCarrier
* Battleship
* Submarine
* Destroyer
* PatrolBoat
Each of these classes has a constructor, the purpose of which is to set the inherited length variable to the correct value, and to initialize the hit array.


* @ Override String getShipType() - Returns a string representation of the class name, e.g., Patrol Boat, for the class PatrolBoat.
* @ Override public String toString() - Returns a single-character String to use in the Ocean's toString method. This method should return x if the ship has been sunk, S if it has not been sunk.
This method can be used to display locations in the ocean that have been shot at; it should not be used to display locations that have not been shot at.

class EmptySea
--------------
You may wonder why EmptySea is a type of Ship. The answer is that the Ocean contains a Ship array, every location of which is, or can be, a reference to some Ship. 
If a particular location is empty, the obvious thing to do is to put a null in that location. This obvious approach has the problem that, every time we look at some location in the array, we have to check if it is null. By putting a non-null value in empty locations, denoting the absence of a ship, we can save all that null checking.

* EmptySea() - This constructor sets the inherited length variable to 1.
* @ Override boolean shootAt(int row, int column) - This method overrides shootAt(int row, int column) that is inherited from Ship, and always returns false to indicate that nothing was hit.
* @ Override boolean isSunk() - This method overrides isSunk() that is inherited from Ship, and always returns false to indicate that you didn't sink anything.
* @ Override public String toString() - Returns a single-character String to use in the Ocean's toString method (see below).
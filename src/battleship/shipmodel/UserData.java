package battleship.shipmodel;

import lombok.Data;
import battleship.Ocean;

/**
 * 
 * This class handles user input data
 *
 */
@Data
public class UserData {
	
	private String row;    // user input row coordinate  
	private String column; // user input column coordinate 

	/**
	 * Checks validity of user input passed to it as a string, catches
	 * exception if input is not a number or if the number is not
	 * within 0-9 inclusive.
	 * 
	 * @param inputStr     String user input value
	 * @return boolean     true if input is valid
	 */
	public boolean isInputValid(String inputStr) {
		try {
			return Integer.parseInt(inputStr) >= 0
					&& Integer.parseInt(inputStr) <= 9;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * @return int     row as an integer
	 */
	public int getIntRow() {
		return Integer.parseInt(row);
	}

	/**
	 * 
	 * @return int     column as an integer
	 */
	public int getIntColumn() {
		return Integer.parseInt(column);
	}

	/**
	 * Displays stats of shots and hits when game ends.
	 *  
	 * @param ocean       an instance of Ocean 
	 * @return String     the stats of game played
	 */
	public String getUserScore(Ocean ocean) {
	
		int totalNoOfShots = ocean.getHitCount() + ocean.getShotsFired();
		String percentageHitRatio = to2DecimalPlaces((double) 100 * ocean.getHitCount() / totalNoOfShots); 
		return "Your game stats:\n--------------------------"
				+ "\n- Number of ships sunk: \t\t"   + ocean.getShipsSunk()
				+ "\n- Shots fired that missed: \t" + ocean.getShotsFired()
				+ "\n- Shots fired that hit: \t\t" + ocean.getHitCount()  
				+ "\n- Hit ratio (%): \t\t" + percentageHitRatio;
	}
	
	/**
	 * limits a real number to 2 decimal places, checking that the hit ratio
	 * is with expected range 0-100. 
	 * 
	 * @param realNumber     a real number such as the hit ratio
	 * @return String        the real number limited to 2 decimal places
	 */
	private String to2DecimalPlaces(double realNumber) {
		
		String result = "";
		
		try {
		
			if (realNumber < 0 || realNumber > 100) {
				throw new IllegalArgumentException();
			} else if (realNumber < 10) {
				result = (""+realNumber).substring(0,1) + "." + (""+realNumber).substring(2,4);
			} else if (realNumber < 100) {
				result = (""+realNumber).substring(0,2) + "." + (""+realNumber).substring(3,5);				
			}
			
		} catch (IllegalArgumentException e) {
			System.out.println("a hit ratio cannot be lower than 0 or higher than 100%");
		}
				
		return result;
	}
}

package battleship.shipmodel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This class is the subclass of Ship. Describes a part of the ocean that
 * doesn't have a ship in it.
 * 
 * @author Teresa.Wu
 */

@Data
@EqualsAndHashCode(callSuper = false, of = { "shipType", "length" })
public class EmptySea extends Ship {
	private static final String SHIP_TYPE = "Emptysea";
	private int length;
	private String shipType;

	/**
	 * this constructor sets the "ship length" to 1
	 */
	public EmptySea() {
		this.setLength(1);
		this.setShipType(SHIP_TYPE);
		hit = new boolean[1];
	}

	/**
	 * return the length of the ship
	 */
	@Override
	public int getLength() {
		return length;
	}

	/**
	 * return a string representation of the class name
	 */
	@Override
	public String getShipType() {
		return shipType;
	}

	/**
	 * return a single-character String to use in the Ocean's toString(), return
	 * "e" to indicates this is Empty sea
	 */
	@Override
	public String toString() {
		return "e";
	}
}

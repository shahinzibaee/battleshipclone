package battleship.shipmodel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This class is the subclass of Ship. Describes a ship with length 2.
 * 
 * @author Teresa.Wu
 */

@Data
@EqualsAndHashCode(callSuper=false, of = { "shipType", "length" })
public class Destroyer extends Ship {
	private static final String SHIP_TYPE = "Destroyer";
	private static final int SHIP_LENGTH = 2;
	private String shipType;
	private int length;

	public Destroyer() {
		this.setShipType(SHIP_TYPE);
		this.setLength(SHIP_LENGTH);
		hit = new boolean[SHIP_LENGTH];
	}

	/**
	 * return the length of the ship
	 */
	@Override
	public int getLength() {
		return length;
	}

	/**
	 * return a string representation of the class name
	 */
	@Override
	public String getShipType() {
		return shipType;
	}

	/**
	 * return a single-character String to use in the Ocean's toString() returns
	 * "x" if the ship has been sunk returns "S" if it has not been sunk
	 */
	@Override
	public String toString() {
		return isSunk() ? "x" : "S";
	}
}

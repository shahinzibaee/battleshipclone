package battleship.shipmodel;

import battleship.Ocean;
import battleship.GameUtil;

/**
 * This class is the superclass of the classes for ships
 * 
 * @author Teresa.Wu
 */
public abstract class Ship {
	// bow row number
	protected int bowRow;
	// bow column number
	protected int bowColumn;
	// returns true if the ship is placed horizontally, false otherwise
	protected boolean isHorizontal;

	/**
	 * return the length of this particular ship
	 */
	public abstract int getLength();

	public abstract String getShipType();

	// a list of hit records, a cell is true if ship was hit, false otherwise
	protected boolean[] hit;
	private Ship ships[][];
	private GameUtil gridUtil;

	/**
	 * returns true if it is okay to put a ship of this length with its bow at
	 * this location, with the given orientation, and returns false otherwise.
	 * The ship must not overlap another ship, or touch another ship, and it
	 * must not stick out beyond the grid.
	 */
	public boolean okToPlaceShipAt(int row, int column, boolean horizontal,
			Ocean ocean) {
		ships = ocean.getShipArray();
		gridUtil = new GameUtil(ships, this);
		try {// check if cell is empty
			if (!(ships[row][column] instanceof EmptySea)
					|| !gridUtil.validateGrid(row, column, horizontal, ocean)) {
				return false;
			}
			return true;
		}
		// check if ship was placed outside the board
		catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	/**
	 * places the ship in the ocean. This involves giving values to the bowRow,
	 * bowColumn, and horizontal instance variables in the ship, and it also
	 * involves putting a reference to the ship in each of 1 or more locations
	 * (up to 4) in the ships grid in the Ocean object.
	 */
	public void placeShipAt(int row, int column, boolean horizontal, Ocean ocean) {
		this.bowRow = row;
		this.bowColumn = column;
		this.isHorizontal = horizontal;
		ships = ocean.getShipArray();
		try {
			for (int i = 0; i < this.getLength(); i++) {
				if (horizontal) {
					ships[row][column + i] = this;
				} else {
					ships[row + i][column] = this;
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
	}

	/**
	 * if a part of the ship occupies the given row and column, and the ship
	 * hasn't been sunk, mark that part of the ship as hit (in the hit array,
	 * where index 0 indicates the bow) and return true, otherwise return false.
	 */
	public boolean shootAt(int row, int column) {
		if (isHorizontal) {
			for (int i = 0; i < this.getLength(); i++) {
				if (bowRow == row && bowColumn + i == column) {
					if (this.isSunk()) {
						return false;
					} else {
						hit[i] = true;
						return true;
					}
				}
			}
		} else {
			for (int i = 0; i < this.getLength(); i++) {
				if (bowRow + i == row && bowColumn == column) {
					if (this.isSunk()) {
						return false;
					} else {
						hit[i] = true;
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * return true if every part of the ship has been hit, false otherwise.
	 */
	public boolean isSunk() {
		for (int i = 0; i < hit.length; i++) {
			if (!hit[i]) {
				return false;
			}
		}
		return true;
	}
}

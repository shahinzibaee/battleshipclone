package battleship;

import static battleship.ShipFactory.buildShip;

import java.util.ArrayList;
import java.util.Random;

import battleship.shipmodel.EmptySea;
import battleship.shipmodel.Ship;

public class Ocean {
	private Ship[][] shipArray; // game board
	private String[][] shipStatusStrArray; // game board string array
	private int numShots; // number of shots
	private int numHits; // number of hits
	private int numSunkShips; // number of ships sunk
	private Ship[] fleet; // store all Ships in the fleet

	private static final int GRID_SIZE = 10; // size of the ocean
	private static final int DEFAULT_FLEET_SIZE = 11; // size of the default
														// flee

	private static final String SHOT_HIT = "[S]";
	private static final String SHOT_NO_HIT = "[-]";
	private static final String SUNK_SHIP = "[X]";
	private static final String NO_SHOT = "[.]";
	private static final int MAX_SHIP_PLACE_TRY = 2000;

	/**
	 * Constructor Creates an empty ocean (fills the ships array with
	 * EmptySeas). Also initializes any game variables, such as how many shots
	 * have been fired.
	 */
	public Ocean() {
		initOcean();
	}

	/**
	 * Place all the ships randomly on the (initially empty) ocean.
	 * 
	 * @throws Exception
	 *             if tried more than max times but not placed.
	 */
	public void placeAllShipsRandomly() throws Exception {
		// check if the game is played; if so initialize the status first
		// if (numShots > 0) {
		initOcean();
		// }

		// place the ships randomly
		Random rand = new Random();
		int numTrys;
		boolean isMoreThanMaxTrys;

		// int testSum = 0;

		for (int i = 0; i < DEFAULT_FLEET_SIZE; i++) {
			Ship s = fleet[i];
			// testSum = testSum + s.getLength();

			numTrys = 0;
			isMoreThanMaxTrys = false;

			while (!isMoreThanMaxTrys) {
				numTrys++;

				// generate row and column number
				int row = rand.nextInt(GRID_SIZE);
				int column = rand.nextInt(GRID_SIZE);
				boolean hori = rand.nextBoolean();

				// System.out.println(row + ":" + column + ":" + s.getLength());
				// check if the location (row, column) can place the ship in
				// either horizontally or vertically
				// otherwise try next location
				if (s.okToPlaceShipAt(row, column, hori, this)) {
					s.placeShipAt(row, column, hori, this);
					break;
				} else if (s.okToPlaceShipAt(row, column, !hori, this)) {
					s.placeShipAt(row, column, !hori, this);
					break;
				}

				// if try more than certain times exit with error throw
				if (numTrys > MAX_SHIP_PLACE_TRY) {
					isMoreThanMaxTrys = true;
				}
			}

			if (isMoreThanMaxTrys) {
				throw new Exception("Placing ship more than max allowed trys");
			}
		}

		// System.out.println(testSum);
	}

	/**
	 * Tell whether the given location contains ship
	 * 
	 * @param row
	 *            int of row number
	 * @param column
	 *            int of column number
	 * 
	 * @return true if the location contains ship or false if the location does
	 *         not contain ship
	 */
	public boolean isOccupied(int row, int column) {
		// check if the ship type of the location equals to the EmptySea's ship
		// type
		return !(shipArray[row][column] instanceof EmptySea);
	} 

	/**
	 * Shoot at the given location, update the number of fires and the number of
	 * hits
	 * 
	 * @param row
	 *            int of row number
	 * @param column
	 *            int of column number
	 * 
	 * @return true if the given location contains a afloat ship or false if
	 *         it's empty sea
	 */
	public boolean shootAt(int row, int column) {

		Ship s = shipArray[row][column];

		if (!isOccupied(row, column)) {
			// mark status array
			shipStatusStrArray[row][column] = SHOT_NO_HIT; // shot but not hit
															// location
			// if it's empty sea then true does not count hit
			if (s.shootAt(row, column)) {
				numShots++; // true means not shot this location before but hit
							// does not count
			}
			return false;
		} else {
			// if it's not empty sea then true does count hit
			if (s.shootAt(row, column)) {
				// true means ship not sunk yet; may or may not hit before

				// check if hit this location before, if not then update all
				// status
				if (shipStatusStrArray[row][column].equals(NO_SHOT)) {
					// mark status array
					shipStatusStrArray[row][column] = SHOT_HIT; // shot and hit
																// a ship

					numShots++;
					numHits++;
					// if ship sunk after hit increase number of sunk ship
					if (s.isSunk()) {
						numSunkShips++;
					}
				}

				return true;
			} else {
				// means this part of ship already hit before and ship sunk
				// already
				return false;
			}
		}
	}

	/**
	 * Give number of shots fired
	 * 
	 * @return number of shots fired
	 */
	public int getShotsFired() {
		return numShots;
	}

	/**
	 * Give number of hits
	 * 
	 * @return number of hits
	 */
	public int getHitCount() {
		return numHits;
	}

	/**
	 * Give number of ships sunk
	 * 
	 * @return number of ships sunk
	 */
	public int getShipsSunk() {
		return numSunkShips;
	}

	/**
	 * Tell if game is over: all ships sunk
	 * 
	 * @return true if all ships sunk or false still ships afloat
	 */
	public boolean isGameOver() {
		if (numSunkShips == DEFAULT_FLEET_SIZE) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Give the game board
	 * 
	 * @return the grid of ships (game board)
	 */
	public Ship[][] getShipArray() {
		return shipArray;
	}

	/**
	 * Give the String presentation of the game board
	 * 
	 * @return the String of the grid of ships (game board)
	 */
	@Override
	public String toString() {
		String shipArrayString = "";

		for (int i = 0; i < GRID_SIZE; i++) {
			for (int j = 0; j < GRID_SIZE; j++) {
				String status;
				if (shipArray[i][j].isSunk() && isOccupied(i, j)) {
					status = SUNK_SHIP;
				} else {
					status = shipStatusStrArray[i][j];
				}
				shipArrayString = shipArrayString + status;
			}
			shipArrayString = shipArrayString + "\n";
		}

		return shipArrayString;
	}

	/**
	 * Initialize the game and set up the variables to new game status
	 */
	private void initOcean() {
		// initialize shipArray with EmptySea
		initShipArray();

		// initialize shipArrayStr with NO_SHOT
		shipStatusStrArray = createNewShipStatusStrArray();

		// set 0s to numShots, numHits and numSunkShips
		numShots = 0;
		numHits = 0;
		numSunkShips = 0;

		// initialize the fleet
		fleet = loadFleet();
	}

	/**
	 * Give an ArralyList of ships in a fleet
	 * 
	 * @return hard coded fleet
	 */
	private Ship[] loadFleet() {
		ArrayList<Ship> fleetArrayList = new ArrayList<Ship>();
		Ship[] fleetArray;

		fleetArrayList.add(buildShip("AircraftCarrier")); // one AircraftCarrier
		fleetArrayList.add(buildShip("Battleship")); // two Battleship
		fleetArrayList.add(buildShip("Battleship")); // two Battleship
		fleetArrayList.add(buildShip("Submarine")); // two Submarine
		fleetArrayList.add(buildShip("Submarine")); // two Submarine
		fleetArrayList.add(buildShip("Destroyer")); // two Destroyer
		fleetArrayList.add(buildShip("Destroyer")); // two Destroyer
		fleetArrayList.add(buildShip("PatrolBoat")); // four PatrolBoat
		fleetArrayList.add(buildShip("PatrolBoat")); // four PatrolBoat
		fleetArrayList.add(buildShip("PatrolBoat")); // four PatrolBoat
		fleetArrayList.add(buildShip("PatrolBoat")); // four PatrolBoat

		fleetArray = new Ship[fleetArrayList.size()];
		for (int i = 0; i < fleetArray.length; i++) {
			fleetArray[i] = fleetArrayList.get(i);
		}

		return fleetArray;
	}

	/**
	 * Give a new 2D array of EmptySea
	 * 
	 * @return a new 2D array of EmptySea
	 */
	private void initShipArray() {
		shipArray = new Ship[GRID_SIZE][GRID_SIZE];

		for (int i = 0; i < GRID_SIZE; i++) {
			for (int j = 0; j < GRID_SIZE; j++) {
				Ship s = buildShip("EmptySea");
				s.placeShipAt(i, j, true, this);
				// shipArray[i][j] = buildShip("EmptySea");
			}
		}

	}

	/**
	 * Give a new 2D array of NO_SHOT
	 * 
	 * @return a new 2D array of status string of NO_SHOT
	 */
	private String[][] createNewShipStatusStrArray() {
		String[][] shipStatusStrArray = new String[GRID_SIZE][GRID_SIZE];

		for (int i = 0; i < GRID_SIZE; i++) {
			for (int j = 0; j < GRID_SIZE; j++) {
				shipStatusStrArray[i][j] = NO_SHOT;
			}
		}

		return shipStatusStrArray;
	}

	// print out placed ships
	public void printShips() {
		String s = "";

		for (int i = 0; i < GRID_SIZE; i++) {
			for (int j = 0; j < GRID_SIZE; j++) {
				String shipStr = "["
						+ shipArray[i][j].getShipType().substring(0, 3) + "]";
				if (shipStr.equals("[Emp]")) {
					shipStr = "[   ]";
				}
				s = s + shipStr;
			}
			s = s + "\n";
		}

		System.out.println(s);
	}
}

package battleship;

import battleship.shipmodel.EmptySea;
import battleship.shipmodel.Ship;

public class GameUtil {
	private Ship[][] ships;
	private Ship ship;

	public GameUtil(Ship[][] ships, Ship ship) {
		this.ships = ships;
		this.ship = ship;
	}

	/**
	 * empty constructor (for BattleshipGame)
	 */
	public GameUtil() {
	}

	/**
	 * check if can place ship on broad with no ships around
	 * 
	 * @return true if can, false otherwise
	 */
	public boolean validateGrid(int row, int column, boolean horizontal,
			Ocean ocean) {
		return canPlaceOnTheLeft(row, column, horizontal, ocean)
				&& canPlaceOnTheRight(row, column, horizontal, ocean)
				&& canPlaceAtTheTop(row, column, horizontal, ocean)
				&& canPlaceAtTheBottom(row, column, horizontal, ocean);
	}

	/**
	 * check if can place a ship in this location, and it wont touch another
	 * ship
	 * 
	 * @return true if it is, false otherwise
	 */
	private boolean canPlaceOnTheLeft(int row, int column, boolean horizontal,
			Ocean ocean) {
		ships = ocean.getShipArray();
		// return true if cell is on the edge
		if (column == 0) {
			return true;
		} else if (horizontal) {
			if (!(ships[row][column - 1] instanceof EmptySea)
					|| !(ships[row + 1][column - 1] instanceof EmptySea)
					|| !(ships[row - 1][column - 1] instanceof EmptySea)) {
				return false;
			}
		}
		return checkRestSurroundingArea(row, column - 1, horizontal, ocean);
	}

	/**
	 * check if can place a ship in this location, and it wont touch another
	 * ship
	 * 
	 * @return true if it is, false otherwise
	 */
	private boolean canPlaceOnTheRight(int row, int column, boolean horizontal,
			Ocean ocean) {
		ships = ocean.getShipArray();
		// ignore cell checking if it went beyond the grid
		if ((!horizontal && column == ships.length - 1)
				|| (horizontal && (column + ship.getLength() - 1) == ships.length - 1)) {
			return true;
		} else if (horizontal) {
			if (!(ships[row][column + ship.getLength()] instanceof EmptySea)
					|| !(ships[row + 1][column + ship.getLength()] instanceof EmptySea)
					|| !(ships[row - 1][column + ship.getLength()] instanceof EmptySea)) {
				return false;
			}
		}
		return checkRestSurroundingArea(row, column + 1, horizontal, ocean);
	}

	/**
	 * check if can place a ship in this location, and it wont touch another
	 * ship
	 * 
	 * @return true if it is, false otherwise
	 */
	private boolean canPlaceAtTheTop(int row, int column, boolean horizontal,
			Ocean ocean) {
		ships = ocean.getShipArray();
		// ignore cell checking if it went beyond the grid
		if (row == 0) {
			return true;
		} else if (!horizontal) {
			if (!(ships[row - 1][column] instanceof EmptySea)
					|| !(ships[row - 1][column - 1] instanceof EmptySea)
					|| !(ships[row - 1][column + 1] instanceof EmptySea)) {
				return false;
			}
		}
		return checkRestSurroundingArea(row - 1, column, horizontal, ocean);
	}

	/**
	 * check if can place a ship in this location, and it wont touch another
	 * ship
	 * 
	 * @return true if it is, false otherwise
	 */
	private boolean canPlaceAtTheBottom(int row, int column,
			boolean horizontal, Ocean ocean) {
		ships = ocean.getShipArray();
		// ignore cell checking if it went beyond the grid
		if ((horizontal && row == ships.length - 1)
				|| (!horizontal && (row + ship.getLength() - 1) == ships.length - 1)) {
			return true;
		} else if (!horizontal) {
			if (!(ships[row + ship.getLength()][column] instanceof EmptySea)
					|| !(ships[row + ship.getLength()][column - 1] instanceof EmptySea)
					|| !(ships[row + ship.getLength()][column + 1] instanceof EmptySea)) {
				return false;
			}
		}
		return checkRestSurroundingArea(row + 1, column, horizontal, ocean);
	}

	// check the rest of surrounding areas
	private boolean checkRestSurroundingArea(int row, int column,
			boolean horizontal, Ocean ocean) {
		for (int i = 0; i < ship.getLength(); i++) {
			if (!(ships[row][column] instanceof EmptySea)) {
				return false;
			}
			if (horizontal) {
				column++;
			} else {
				row++;
			}
		}
		return true;
	}
}

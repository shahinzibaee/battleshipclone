package battleship.junit.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import battleship.Ocean;
import battleship.ShipFactory;
import battleship.shipmodel.UserData;

//@Data
public class UserDataTesting {
	
	private UserData data;
	private Ocean ocean;

	@Before
	public void setUp() throws Exception {
		data = new UserData();
	}

	@After
	public void tearDown() throws Exception {
		data = null;
	}
	
	/**
	 * test user input
	 */
	@Test
	public void testisInputValidString() {
		int rowOrColumnMinimum = 0;
		int rowOrColumnMaximum = ShipFactory.buildOcean().getShipArray().length-1;
		boolean actualOutput = true;
		
		for(int i=rowOrColumnMinimum;i<=rowOrColumnMaximum;i++) {
			String input = ""+i;
			actualOutput = actualOutput && data.isInputValid(input);
		}	
		assertTrue(actualOutput);
		
		assertFalse(data.isInputValid("a"));
		assertFalse(data.isInputValid("O"));
		assertFalse(data.isInputValid("10"));
		assertFalse(data.isInputValid("123"));
		assertFalse(data.isInputValid("*"));
		assertFalse(data.isInputValid("-1"));
	}

	/**
	 * test get Integer - row number
	 */
	@Test
	public void testGetIntRowString() {
		data.setRow("5");
		assertEquals(5, data.getIntRow());
	}

	/**
	 * test get Integer - column number
	 */
	@Test
	public void testGetIntColumnString() {
		data.setColumn("3");
		assertEquals(3, data.getIntColumn());
	}

	/**
	 * tests the display to UI for final score and that the stats are 
	 * calculated and presented correctly.
	 */
	@Test
	public void testGetUserScoreOcean() {

		int mockNumOfShotsFired = 101;
		int mockNumOfHits = 27;
		int mockNumOfShipsSunk = 11;
		double mockPercentageHitRatio = 21.09; // this is (27/(101+27)) to 2 decimal places
		
		ocean = mock(Ocean.class);
		when(ocean.getShotsFired()).thenReturn(mockNumOfShotsFired);
		when(ocean.getHitCount()).thenReturn(mockNumOfHits);
		when(ocean.getShipsSunk()).thenReturn(mockNumOfShipsSunk);
		
		String expectedOutput = "Your game stats:\n--------------------------"
		+ "\n- Number of ships sunk: \t\t" + mockNumOfShipsSunk 
		+ "\n- Shots fired that missed: \t" + mockNumOfShotsFired 
		+ "\n- Shots fired that hit: \t\t" + mockNumOfHits
		+ "\n- Hit ratio (%): \t\t" + mockPercentageHitRatio;

		String actualOutput = data.getUserScore(ocean);

		assertEquals(expectedOutput,actualOutput);
	
	}

	/**
	 * To pass this test requires temporarily making this private method public 
	 * (Hence the test fails while visibility is private)
	 */
	@Test
	public void testto2DecimalPlacesDouble() {
		
		double inputDouble = 23.45345;
		double inputDouble2 = 3.45345;
//		double inputDouble3 = 123.45345;
//		double inputDouble4 = -1.45345;
		
		String actualOutput = data.to2DecimalPlaces(inputDouble);
		String actualOutput2 = data.to2DecimalPlaces(inputDouble2);
//		String actualOutput3 = data.to2DecimalPlaces(inputDouble3); // correctly throws exception
//		String actualOutput4 = data.to2DecimalPlaces(inputDouble4); // correctly throws exception

		String expectedOutput = "23.45";
		String expectedOutput2 = "3.45";
		
		assertEquals(expectedOutput,actualOutput);
		assertEquals(expectedOutput2,actualOutput2);

	}
}
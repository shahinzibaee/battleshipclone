package battleship.junit.testing;

import static battleship.ShipFactory.buildShip;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import battleship.Ocean;
import battleship.shipmodel.Ship;

public class OceanTest {
	private Ocean ocean = new Ocean();

	@Test
	public void constructorTest() {

		assertEquals(ocean.getShotsFired(), 0);
		assertEquals(ocean.getHitCount(), 0);
		assertEquals(ocean.getShipsSunk(), 0);

		// after init should be all empty sea
		Ship tempEmptySea = buildShip("EmptySea");
		Ship[][] shipArray = ocean.getShipArray();
		for (int i = 0; i < shipArray.length; i++) {
			for (int j = 0; j < shipArray.length; j++) {
				Ship s = shipArray[i][j];
				assertEquals(s.getShipType(), tempEmptySea.getShipType());
			}
		}

		//String ships = ocean.toString();
		//System.out.println(ships);
	}

	@Test
	public void placeAllShipsRandomlyTest() {
		try{
			ocean.placeAllShipsRandomly();
		
			Ship[][] shipArray1 = ocean.getShipArray();
			String locations1 = "";
			int numShip1 = 0;
			for (int i = 0; i < shipArray1.length; i++) {
				for (int j = 0; j < shipArray1.length; j++) {
					Ship s = shipArray1[i][j];
					if (ocean.isOccupied(i, j)) {
						numShip1++;
						locations1 = locations1 + "[" + i + "," + j + ": "
								+ s.getShipType() + "]";
					}
				}
			}
	
			// should have 27 parts of ships on the board
			assertEquals(numShip1, 27);
	
			// second round should return different shipArray
			ocean.placeAllShipsRandomly();
			Ship[][] shipArray2 = ocean.getShipArray();
			String locations2 = "";
			int numShip2 = 0;
			for (int i = 0; i < shipArray2.length; i++) {
				for (int j = 0; j < shipArray2.length; j++) {
					Ship s = shipArray2[i][j];
					if (ocean.isOccupied(i, j)) {
						numShip2++;
						locations2 = locations2 + "[" + i + "," + j + ": "
								+ s.getShipType() + "]";
					}
				}
			}
	
			// should have the same number of parts on ships on the board
			assertEquals(numShip1, numShip2);
	
			// two locations strings shouldn't match
			assertFalse(locations1.equals(locations2));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Reached max tries");
		}
	}

	@Test
	public void isOccupiedTest() {
		Ocean newOcean = new Ocean();
		int numOccupied = 0;

		Ship[][] shipArray = newOcean.getShipArray();

		for (int i = 0; i < shipArray.length; i++) {
			for (int j = 0; j < shipArray.length; j++) {
				if (newOcean.isOccupied(i, j)) {
					numOccupied++;
				}
			}
		}

		assertEquals(numOccupied, 0);

		numOccupied = 0;
		try{
			newOcean.placeAllShipsRandomly();
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (newOcean.isOccupied(i, j)) {
						numOccupied++;
						//System.out.println(i + ":" + j);
					}
				}
			}
	
			assertEquals(numOccupied, 27);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Reached max tries");
		}
	}

	@Test
	public void shootAtTest() {
		try{
			ocean.placeAllShipsRandomly();
			
			Ship[][] shipArray = ocean.getShipArray();
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (!ocean.isOccupied(i, j)) {
						assertFalse(ocean.shootAt(i, j));
					} else {
						assertTrue(ocean.shootAt(i, j));
					}
				}
			}
	
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					assertFalse(ocean.shootAt(i, j));
				}
			}
	
			ocean.placeAllShipsRandomly();
			Ship tempAircraftCarrier = buildShip("AircraftCarrier");
			shipArray = ocean.getShipArray();
			int numHit = 0;
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (shipArray[i][j].getShipType().equals(
							tempAircraftCarrier.getShipType())
							&& numHit < 1) {
						assertTrue(ocean.shootAt(i, j));
						assertTrue(ocean.shootAt(i, j));
						numHit++;
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Reached max tries");
		}
	}

	@Test
	public void getShotsFiredTest() {
		try{
			ocean.placeAllShipsRandomly();
			assertEquals(ocean.getShotsFired(), 0);
	
			int expectShots = 10;
			int numShots = 0;
			Ship[][] shipArray = ocean.getShipArray();
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (numShots < expectShots) {
						numShots++;
						ocean.shootAt(i, j);
					}
				}
			}
	
			assertEquals(ocean.getShotsFired(), expectShots);
	
			numShots = 0;
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (numShots < expectShots) {
						numShots++;
						ocean.shootAt(i, j);
					}
				}
			}
	
			assertEquals(ocean.getShotsFired(), expectShots);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Reached max tries");
		}
	}

	@Test
	public void getHitCountTest() {
		try{
			ocean.placeAllShipsRandomly();
			assertEquals(ocean.getHitCount(), 0);
			
			Ship[][] shipArray = ocean.getShipArray();
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (!ocean.isOccupied(i, j)) {
						ocean.shootAt(i, j);
					}
				}
			}
	
			assertEquals(ocean.getHitCount(), 0);
	
			int expectHits = 11;
			int numHits = 0;
	
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (ocean.isOccupied(i, j) && numHits < expectHits) {
						numHits++;
						ocean.shootAt(i, j);
					}
				}
			}
	
			assertEquals(ocean.getHitCount(), expectHits);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Reached max tries");
		}
	}

	@Test
	public void getShipsSunkTest() {
		try{
			ocean.placeAllShipsRandomly();
			assertEquals(ocean.getShipsSunk(), 0);
	
			Ship[][] shipArray = ocean.getShipArray();
			Ship tempAircraftCarrier = buildShip("AircraftCarrier");
	
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (shipArray[i][j].getShipType().equals(
							tempAircraftCarrier.getShipType())) {
						ocean.shootAt(i, j);
					}
				}
			}
	
			assertEquals(ocean.getShipsSunk(), 1);
	
			int numShots = 0;
	
			Ship tempBattleship = buildShip("Battleship");
			for (int i = 0; i < shipArray.length; i++) {
				for (int j = 0; j < shipArray.length; j++) {
					if (shipArray[i][j].getShipType().equals(
							tempBattleship.getShipType())) {
						numShots++;
						if (numShots == 1) {
							assertEquals(ocean.getShipsSunk(), 1);
						}
						ocean.shootAt(i, j);
					}
				}
			}
	
			assertEquals(ocean.getShipsSunk(), 3);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Reached max tries");
		}
	}

	@Test
	public void isGameOverTest() {
		try{
			ocean.placeAllShipsRandomly();
		}
		catch(Exception e){
			fail("Reached max tries");
		}
		
		assertFalse(ocean.isGameOver());
		
		Ship[][] shipArray = ocean.getShipArray();

		for (int i = 0; i < shipArray.length; i++) {
			for (int j = 0; j < shipArray.length; j++) {
				if (ocean.isOccupied(i, j)) {
					ocean.shootAt(i, j);
				}
			}
		}

		assertTrue(ocean.isGameOver());
	}
	
	@Test
	public void cheatingToPrintShips(){
		Ocean o = new Ocean();
		try{
			o.placeAllShipsRandomly();
		}
		catch(Exception e){
			System.out.println("Max Hit");
		}
		o.printShips();
	}
}

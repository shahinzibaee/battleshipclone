package battleship.junit.testing;

import static battleship.ShipFactory.buildShip;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import lombok.Data;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import battleship.shipmodel.AircraftCarrier;
import battleship.shipmodel.Battleship;
import battleship.shipmodel.Destroyer;
import battleship.shipmodel.EmptySea;
import battleship.shipmodel.PatrolBoat;
import battleship.shipmodel.Ship;
import battleship.shipmodel.Submarine;

@Data
public class ShipModelTesting {
	private Ship ship;
	private Ship[] shipList;
	private String[] shipTypeStrings, shipClass;
	private int[] shipLength;

	@Before
	public void methodBefore() {
		shipTypeStrings = new String[] { "AircraftCarrier", "Battleship",
				"Destroyer", "EmptySea", "PatrolBoat", "Submarine" };
		shipClass = new String[shipTypeStrings.length];
		for (int i = 0; i < shipTypeStrings.length; i++) {
			shipClass[i] = shipTypeStrings[i];
		}
		shipLength = new int[] { 5, 4, 2, 1, 1, 3 };
		shipList = new Ship[] { new AircraftCarrier(), new Battleship(),
				new Destroyer(), new EmptySea(), new PatrolBoat(),
				new Submarine() };
	}

	/**
	 * test mocking
	 */
	@Test
	public void testMockingEmptySea() {
		ship = mock(EmptySea.class);
		Ship anotherShip = buildShip("EmptySea");
		when(ship.getShipType()).thenReturn("Emptysea");
		when(ship.getLength()).thenReturn(1).thenReturn(0);
		assertEquals(anotherShip.getShipType(), ship.getShipType());
		assertEquals(anotherShip.getLength(), ship.getLength());
		verify(ship).getLength();
	}

	/**
	 * this method test ship type
	 */
	@Test
	public void testShipType() {
		for (int i = 0; i < shipTypeStrings.length; i++) {
			ship = buildShip(shipClass[i]);
			assertTrue(shipTypeStrings[i],
					ship.getShipType().equalsIgnoreCase(shipTypeStrings[i]));
		}
	}

	/**
	 * this method test equals and hashcode
	 */
	@Test
	public void testShipEquals() {
		for (int i = 0; i < shipLength.length; i++) {
			ship = buildShip(shipClass[i]);
			assertTrue(shipTypeStrings[i], shipList[i].equals(ship));
		}
	}

	/**
	 * this method test ship length
	 */
	@Test
	public void testShipLength() {
		for (int i = 0; i < shipLength.length; i++) {
			ship = buildShip(shipClass[i]);
			assertTrue(shipTypeStrings[i], ship.getLength() == shipLength[i]);
		}
	}
	
	@After
	public void methodAfter() {
		shipLength = null;
		shipTypeStrings = null;
		ship = null;
		shipList = null;
	}

}

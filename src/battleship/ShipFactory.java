package battleship;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import battleship.shipmodel.Ship;

/**
 * This class is the factory class to build a new ship
 * 
 * @author Teresa.Wu
 */
public class ShipFactory {
	//PREFIX is the absolute path to the given ship class
	private static final String PREFIX = "battleship.shipmodel.";
	/**
	 * @param className
	 *            is a string represents the ship type
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * 
	 * @return a new instance of given class by class name
	 */
	public static Ship buildShip(String className) {
		Constructor constructor = null;
		Class shipClass = null;
		try {		
			if (className == null||className.length()==0) {
				System.out.println("Syntax Error: Please provide a class name");
			} else {
				shipClass = Class.forName(PREFIX + className);
				constructor = shipClass.getConstructor(new Class[] {});
				return (Ship) constructor.newInstance();
			}
		} catch (ClassNotFoundException e1) {
			System.out.println("Syntax Error: Class name not found");
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException | InvocationTargetException
				| InstantiationException | IllegalAccessException e) {
			System.out.println("Syntax Error: Parameters don't match");
		}
		return null;
	}
	
	/**
	 * @return a new instance of Ocean
	 */
	public static Ocean buildOcean() {
		return new Ocean();
	}
}

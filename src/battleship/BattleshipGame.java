package battleship;

import static battleship.ShipFactory.buildOcean;
import java.util.Scanner;
import battleship.shipmodel.UserData;

public class BattleshipGame {

	private Ocean ocean;
	private UserData data;

	public static void main(String[] args) {
		
		BattleshipGame game = new BattleshipGame();
		game.init();
	
	}

	/**
	 * Builds an ocean via buildOceanGrid()), populates ocean with ships via
	 * addShips() and displays an invitation to UI to play game via 
	 * inviteUserToShoot(). 
	 */
	public void init() {
	
		buildOceanGrid();
		addShips();
		inviteUserToShoot();
	
	}

	/**
	 * Generates and displays ocean grid (10x10 grid of 100 EmptySea objects).
	 * Instantiates input validation code, UserData. 
	 * Generates ocean grid via ShipFactory's buildOcean().
	 */
	private void buildOceanGrid() {
	
		ocean = buildOcean();
		System.out.println(ocean);
		data = new UserData();
	
	}

	/**
	 * Instructs ocean to populate itself with a fleet of ships at random positions 
	 * in the empty ocean, via Ocean's placeAllShipsRandomly().
	 */
	private void addShips() {
	
		try {
			ocean.placeAllShipsRandomly();
		} catch (Exception e) {
			System.out.println("Having difficulty placing all ships in ocean");
			e.printStackTrace();
		}
	
	}

	/**
	 * Displays welcome message and mediates input/output to UI via processInput(row or column) 
	 * and getResultOfShot(). 
	 */
	private void inviteUserToShoot() {
	
		System.out.println("Welcome to Battleship game");
	
		do {
			data.setRow(processInput("row"));
			data.setColumn(processInput("column"));
			
			if (data.getRow() != null && data.getColumn() != null) {
				getResultOfShot();
			}	
			
		} while (!ocean.isGameOver());

	}

	/**
	 * Displays prompt for shot positional coordinates (row, then column) to UI. 
	 * Computes outcome with valid numeric inputs, within 0-9.
	 *
	 * Input validation performed via isInputValid(user's input) in shipmodel.UserData.
	 * 
	 * @param rowOrString     a String that is either row or column
	 */
	private String processInput(String rowOrString) {

		Scanner scan = new Scanner(System.in);
		String inputStr = "";
		
		do {
			System.out.println("pick " + rowOrString +  " (0-9): ");
			inputStr = scan.next().toString();
		} while (!data.isInputValid(inputStr));
		
		if (ocean.isGameOver()) {
			scan.close();
		}
		
		return inputStr;
		
	}

	/**
	 * Computes and displays result (hit/miss) of a shot to the UI, after every shot. 
	 * Computation performed via Ocean's isOccupied(row, column).
	 * 
	 * Updates the array of ships in ocean via updateShips(row,column).  
	 * 
	 * Computes and displays if ship has just been sunk and what type of ship it was. 
	 * Computation performed via Ocean's getShipArray(row,column).isSunk() and 
	 * getShipArray(row,column).getShipType().
	 * 
	 * Displays final results at game end, followed by an invitation play again, 
	 * via reinviteUser().
	 * Computes game end via Ocean's isGameOver().
	 * 
	 * @param row        ordinate (0-9) position of current shot
	 * @param column     abscissa (0-9) position of current shot
	 */
	private void getResultOfShot() {

		if (ocean.isOccupied(data.getIntRow(), data.getIntColumn())) {
			System.out.println("<smash> ..you hit something ! ");
		} else {
			System.out.println("<splash> ..you missed.. ");
		}
		
		updateShips(data.getIntRow(), data.getIntColumn());
		System.out.println(ocean);

		if (ocean.getShipArray()[data.getIntRow()][data.getIntColumn()]
				.isSunk() && !ocean.getShipArray()[data.getIntRow()][data.getIntColumn()]
						.getShipType().equals("Emptysea")) {
			System.out.println("you just sank one "
					+ ocean.getShipArray()[data.getIntRow()][data
							.getIntColumn()].getShipType() + "..!!");
		}

		if (ocean.isGameOver()) {
			System.out.println(data.getUserScore(ocean));
			reinviteUser();
		}
		
	}

	/**
	 * Updates status of ships in the ocean after each shot to the specified 
	 * coordinates (row,column), via Ocean's shootAt(row,column).
	 * 
	 * @param row        ordinate (0-9) position of current shot
	 * @param column     abscissa (0-9) position of current shot
	 */
	private void updateShips(int row, int column) {
		
		ocean.shootAt(row, column);
	
	}

	/**
	 * Displays invitation to play again. Computes user input to either 
	 * re-populate ocean grid for a new game via Ocean's placeAllShipsRandomly() 
	 * or displays acknowledgement to UI and terminates.
	 */
	private void reinviteUser() {
		
		System.out.println("Want to play again? (y/n) ");
		Scanner scan = new Scanner(System.in);
		String result = scan.next().toString().toLowerCase();
		
		if (result.startsWith("y")) {
			
			try {
				ocean.placeAllShipsRandomly();
			} catch (Exception e) {
				System.out
						.println("Having difficulty placing all ships in ocean");
				e.printStackTrace();
			}
			
		} else if (result.startsWith("n")) {
			System.out.println("Goodbye");
			ocean = null;
			scan.close();
			System.exit(0);
		}
		
	}
	
}
